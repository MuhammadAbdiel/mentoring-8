@extends('layout.main')

@section('content')
<h2 class="mt-3">Data Mahasiswa</h2>
<form action="{{ route('import') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
        <label for="file" class="form-label">Import Data</label>
        <input class="form-control" type="file" id="file" name="file">
        <button type="submit" class="btn btn-primary mt-3">Import</button>
    </div>
</form>
<a href="{{ route('export') }}" class="btn btn-success">Export</a>
<table class="table table-striped mt-3">
    <thead>
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Nama Mahasiswa</th>
            <th scope="col">Fakultas</th>
            <th scope="col">Prodi</th>
            <th scope="col">No. Telepon</th>
            <th scope="col">Jenis Kelamin</th>
            <th scope="col">Alamat</th>
            <th scope="col">Tanggal Lahir</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($mahasiswa as $data)
        <tr>
            <th scope="row">{{ $loop->iteration }}</th>
            <td>{{ $data->nama }}</td>
            <td>{{ $data->fakultas }}</td>
            <td>{{ $data->prodi }}</td>
            <td>{{ $data->no_telp }}</td>
            <td>{{ $data->jenis_kelamin }}</td>
            <td>{{ $data->alamat }}</td>
            <td>{{ $data->tanggal_lahir }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection