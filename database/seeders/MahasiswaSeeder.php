<?php

namespace Database\Seeders;

use App\Models\Mahasiswa;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mahasiswa = [
            [
                "nama" => "Rizky Khapidsyah",
                "fakultas" => "Teknik",
                "prodi" => "Teknik Informatika",
                "no_telp" => "081234567890",
                "jenis_kelamin" => "L",
                "alamat" => "Jl. Raya No. 123",
                "tanggal_lahir" => "2000-01-01",
            ],
            [
                "nama" => "Rizky Khapidsyah",
                "fakultas" => "Teknik",
                "prodi" => "Teknik Informatika",
                "no_telp" => "081234567890",
                "jenis_kelamin" => "L",
                "alamat" => "Jl. Raya No. 123",
                "tanggal_lahir" => "2000-01-01",
            ],
            [
                "nama" => "Rizky Khapidsyah",
                "fakultas" => "Teknik",
                "prodi" => "Teknik Informatika",
                "no_telp" => "081234567890",
                "jenis_kelamin" => "L",
                "alamat" => "Jl. Raya No. 123",
                "tanggal_lahir" => "2000-01-01",
            ],
            [
                "nama" => "Rizky Khapidsyah",
                "fakultas" => "Teknik",
                "prodi" => "Teknik Informatika",
                "no_telp" => "081234567890",
                "jenis_kelamin" => "L",
                "alamat" => "Jl. Raya No. 123",
                "tanggal_lahir" => "2000-01-01",
            ],
            [
                "nama" => "Rizky Khapidsyah",
                "fakultas" => "Teknik",
                "prodi" => "Teknik Informatika",
                "no_telp" => "081234567890",
                "jenis_kelamin" => "L",
                "alamat" => "Jl. Raya No. 123",
                "tanggal_lahir" => "2000-01-01",
            ],
        ];

        Mahasiswa::insert($mahasiswa);
    }
}
