<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', MahasiswaController::class)->names([
    'index' => 'mahasiswa.index',
    'store' => 'mahasiswa.store',
    'update' => 'mahasiswa.update',
    'destroy' => 'mahasiswa.destroy',
]);
Route::get('/export', [MahasiswaController::class, 'export'])->name('export');
Route::post('/import', [MahasiswaController::class, 'import'])->name('import');
